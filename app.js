const express = require('express'),
    path = require('path'),
    passport = require('passport'),
    logger = require('morgan'),

    cError = require('./helpers/cerror'),
    cfg = require('./helpers/config'),

    indexRouter = require('./routes/index'),
    usersRouter = require('./routes/users'),

    personApi = require('./routes/api/person'),
    organizationApi = require('./routes/api/organization'),
    issuerApi = require('./routes/api/issuer'),
    badgeApi = require('./routes/api/badge'),
    recipientApi = require('./routes/api/recipient'),
    assertionApi = require('./routes/api/assertion');

// MongoDB
require("./helpers/db")();

// Express
const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json({limit: '1mb'}));
app.use(express.urlencoded({extended: false, limit: '1mb'}));
app.use(express.static(path.join(__dirname, 'public')));

app.use(passport.initialize({}));
require('./helpers/passport');

app.use('/', indexRouter);
app.use('/users', usersRouter);

app.use('/api/person', personApi);
app.use('/api/organizations', organizationApi);
app.use('/api/issuers', issuerApi);
app.use('/api/badges', badgeApi);
app.use('/api/recipients', recipientApi);
app.use('/api/assertions', assertionApi);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(new cError(404, 'Not found'));
});

// error handler
app.use(function (err, req, res, next) {
    // console.error(err);
    // set locals, only providing error in development
    // noinspection JSUnresolvedVariable
    res.locals.message = err.message;
    // noinspection JSUnresolvedVariable
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    // render the error page
    // noinspection JSUnresolvedFunction,JSUnresolvedVariable
    res.status(err.status || 500).json({message: err.message});
});

module.exports = app;
