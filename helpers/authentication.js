// Dependencies
let passport = require('passport'),
    cfg = require('./config');

function isAdmin(req, res, next) {
    passport.authenticate('jwt', cfg.jwtSession, function (err, person, info) {
        if (!err && !info && person) {
            if (person.type === 'Admin') {
                req.person = person;
                return next();
            }
            return res.status(403).json({message: 'Admin access required to perform this action.'});
        }
        return res.status(401).json({message: 'Session expired. Please retrieve account again.'});
    })(req, res, next);
}

function isManager(req, res, next) {
    const types = ['Admin', 'Manager'];
    passport.authenticate('jwt', cfg.jwtSession, function (err, person, info) {
        if (!err && !info && person) {
            if (types.indexOf(person.type) >= 0) {
                req.person = person;
                return next();
            }
            return res.status(403).json({message: 'Manager access required to perform this action.'});
        }
        return res.status(401).json({message: 'Session expired. Please retrieve account again.'});
    })(req, res, next);
}

function isAuth(req, res, next) {
    passport.authenticate('jwt', cfg.jwtSession, function (err, person, info) {
        if (!err && !info && person)
            return next();
        return res.status(401).json({message: 'Session expired. Please retrieve account again.'});
    })(req, res, next);
}

// will not work without login, same as isAuth but also returns User data
function isAuthUser(req, res, next) {
    // console.log(req.headers);
    passport.authenticate('jwt', cfg.jwtSession, function (err, person, info) {
        if ((!err || !info) && person) {
            req.person = person;
            return next();
        }
        return res.status(401).json({message: 'Session expired. Please login again.'});
    })(req, res, next);
}

// will work without login (for public view)
function getAuthUser(req, res, next) {
    passport.authenticate('jwt', cfg.jwtSession, function (err, person, info) {
        if ((!err || !info) && person) {
            req.isLoggedIn = true;
            req.person = person;
        }
        return next();
    })(req, res, next);
}

// use for registration
function isNotAuth(req, res, next) {
    if (!req.isAuthenticated())
        return next();
    return res.status(400).json({message: 'Already logged in.'});
}

module.exports = {
    isAdmin: isAdmin,
    isManager: isManager,
    isAuth: isAuth,
    isAuthUser: isAuthUser,
    getAuthUser: getAuthUser,
    isNotAuth: isNotAuth
};
