class CError extends Error {
    constructor(code, message) {
        // noinspection JSCheckFunctionSignatures
        super(message);
        this.status = code || 500;
        this.name = 'cError';
        // noinspection JSUnresolvedFunction
        Error.captureStackTrace(this, CError);
    }
}

module.exports = CError;
