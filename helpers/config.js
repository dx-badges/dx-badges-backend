module.exports = {
    logType: 'dev',

    jwtSession: {session: false},
    jwtSecret: "Cf+CE&3L_ZaMSt$*wLC!JA-yXq&Z&2ubyN9#H@vjMUYRqy5tqx?xdGzSWDc6EZa&",
    jwtIssuer: 'https://fresnostate.edu',
    jwtAudience: 'fresnostate.edu',

    database: `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASS}@${process.env.MONGO_IP}:${process.env.MONGO_PORT}/dxbadge?authSource=dxbadge`,

    personSecuredFields: '_id email name image telephone description type security issuerOf',
    personPublicFields: '_id email name image telephone description issuerOf',
}
