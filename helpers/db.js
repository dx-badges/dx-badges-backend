// Dependencies
const mongoose = require('mongoose'),
    chalk = require('chalk');

const databaseUrl = require('./config').database;

// noinspection JSUnresolvedVariable
const connected = chalk.bold.cyan,
    error = chalk.bold.yellow,
    disconnected = chalk.bold.red,
    termination = chalk.bold.magenta;

module.exports = () => {

    mongoose.connect(databaseUrl, {
        useCreateIndex: true,
        useNewUrlParser: true,
        useFindAndModify: false,
        useUnifiedTopology: true
    });

    mongoose.connection.on('connected', () => console.log(connected("Mongoose default connection is open to ", databaseUrl)));

    mongoose.connection.on('error', err => console.log(error("Mongoose default connection has occurred " + err + " error")));

    mongoose.connection.on('disconnected', () => console.log(disconnected("Mongoose default connection is disconnected")));

    process.on('SIGINT', () =>
        mongoose.connection.close(() => {
            console.log(termination("Mongoose default connection is disconnected due to application termination"));
            process.exit(0)
        })
    );
};
