const DEFAULT_PAGE = 0, DEFAULT_LIMIT = 20, MAX_LIMIT = 40;

const asyncMiddleware = fn => (req, res, next) => {
    Promise.resolve(fn(req, res, next)).catch(next);
};

const pagination = () => (req, res, next) => {
    let page = parseInt(req.query.page) || DEFAULT_PAGE
    let limit = parseInt(req.query.limit) || DEFAULT_LIMIT;
    if (limit > MAX_LIMIT) limit = MAX_LIMIT;
    if (limit < 1) limit = DEFAULT_LIMIT;
    if (page < DEFAULT_PAGE) page = DEFAULT_PAGE;
    req.limit = limit;
    req.skip = page * limit;
    next()
};

const sort = () => (req, res, next) => {
    req.sort = null;
    if (req.query.sort) {
        if (req.query.direction === 'asc') req.sort = '-' + req.query.sort;
        else if (req.query.direction === 'desc') req.sort = req.query.sort;
    }
    next()
};

module.exports = {
    sort: sort,
    pagination: pagination,
    asyncMiddleware: asyncMiddleware,
};
