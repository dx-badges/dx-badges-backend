// Dependencies
let Person = require('../models/person'),
    passport = require('passport'),
    JwtExtract = require('passport-jwt').ExtractJwt,
    JwtStrategy = require('passport-jwt').Strategy,
    {personSecuredFields, jwtSecret, jwtAudience, jwtIssuer} = require('./config');

passport.serializeUser((user, done) => {
    done(null, user);
});
passport.deserializeUser((user, done) => {
    done(null, user);
});

// not used for login, only used to check authorization
passport.use('jwt', new JwtStrategy({
        secretOrKey: jwtSecret,
        jwtFromRequest: JwtExtract.fromAuthHeaderAsBearerToken(),
        issuer: jwtIssuer,
        audience: jwtAudience,
        passReqToCallback: true
    }, (req, jwt_payload, next) => {
        Person.findById(jwt_payload._id)
            .select(personSecuredFields)
            .exec()
            .then(person => {
                if (person) {
                    // TODO check secret
                    return next(null, person);
                } else return next(null, false);
            })
            .catch(err => next(err, false));
    }
));
