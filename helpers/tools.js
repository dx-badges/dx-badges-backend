const {logType} = require('./config'),
    crypto = require('crypto'),
    chalk = require('chalk'),
    nodeMailer = require('nodemailer');

const success = chalk.bold.green,
    error = chalk.bold.red,
    warning = chalk.bold.yellow,
    info = chalk.bold.cyan;

const printLogs = (...msg) => {
    if (logType === 'dev') console.log(success(msg.join(' ')));
};

const printInfo = (...msg) => {
    if (logType === 'dev') console.log(info(msg.join(' ')));
};

const printWarning = (...msg) => {
    if (logType === 'dev') console.log(warning(msg.join(' ')));
};

const printErrors = (name, ...msg) => {
    if (logType === 'dev') console.log(error('\t' + name, msg.join(' ')));
};
const createSlug = slug => slug.replace(/[^\w\s]/gi, '').trim().toLowerCase().replace(/\W+/g, '-');

const mongoErrorMessage = (err) => {
    let msg = '';
    for (const e of Object.values(err)) msg += e.message + ', ';
    msg = msg.substring(0, msg.length - 2);
    return msg;
};

const generateSecurity = (
    length = 20,
    wishlist = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz~!@-#$'
) =>
    Array.from(crypto.randomFillSync(new Uint32Array(length)))
        .map((x) => wishlist[x % wishlist.length])
        .join('')

const randomInteger = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min

const sendEmail = (recipient, subject, body, attachment = null, options = {isHtml: true}) => {
    return new Promise((resolve, reject) => {
        const port = parseInt(process.env.AWS_SMPT_PORT_3);
        const transporter = nodeMailer.createTransport({
            host: process.env.AWS_SMPT_END_POINT,
            port: port,
            secure: port === 465,
            auth: {
                user: process.env.AWS_SMPT_USER,
                pass: process.env.AWS_SMPT_PWD
            }
        });
        const mailOptions = {
            from: 'dx@mail.fresnostate.edu',
            to: recipient,
            subject: subject
        };
        // noinspection JSUnresolvedVariable
        if (options && options.isHtml) {
            mailOptions.html = body;
        }
        // noinspection JSUnresolvedVariable
        if (options && options.isText) {
            mailOptions.text = body;
        }
        transporter.sendMail(mailOptions, function (err, result) {
            if (err) {
                reject(err);
            } else {
                resolve(result);
            }
        });
    });
}

module.exports = {
    sendEmail: sendEmail,
    printLogs: printLogs,
    printInfo: printInfo,
    createSlug: createSlug,
    printErrors: printErrors,
    printWarning: printWarning,
    mongoErrorMessage: mongoErrorMessage,
    generateSecurity: generateSecurity,
    randomInteger: randomInteger
};
