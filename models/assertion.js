// Dependencies
const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = mongoose.Types.ObjectId,
    cError = require('../helpers/cerror');

mongoose.Promise = require('bluebird');

const AssertionSchema = new Schema({
    recipient: {type: ObjectId, ref: 'Person', required: [true, 'Must specify a recipient'], index: true},
    badge: {type: ObjectId, ref: 'Badge', required: [true, 'Must specify a badge'], index: true},
    verification: {type: String}, // TODO fix structure
    issuedOn: {type: Date, default: Date.now},
    expires: {type: Date, min: Date.now},
    evidence: {type: String}, // TODO fix structure
    narrative: {type: String}, // TODO fix structure
    isRevoked: {type: Boolean, default: false},
    revocationReason: {type: String, trim: true},
});

// Return model
module.exports = mongoose.model('Assertion', AssertionSchema);
