// Dependencies
const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = mongoose.Types.ObjectId,
    cError = require('../helpers/cerror');

mongoose.Promise = require('bluebird');

const BadgeSchema = new Schema({
    name: {type: String, required: [true, 'Must specify a name'], trim: true},
    image: {type: String},
    criteria: {type: String},   // TODO fix structure
    alignment: {type: String},  // TODO fix structure
    tags: [{type: String}],     // TODO fix structure
    expirationDate: {type: Date},
    isDeleted: {type: Boolean, default: false},
    issuer: {type: ObjectId, ref: 'Issuer', required: [true, 'Must specify an issuer'], index: true},
    description: {type: String, trim: true},
});

const autoPopulate = function (next) {
    this.populate({path: 'issuer'});
    next();
};

// noinspection JSUnresolvedFunction
BadgeSchema
    .pre('findOne', autoPopulate)
    .pre('find', autoPopulate);

// Return model
module.exports = mongoose.model('Badge', BadgeSchema);
