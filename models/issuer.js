// Dependencies
const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = mongoose.Types.ObjectId,
    cError = require('../helpers/cerror');

mongoose.Promise = require('bluebird');

const IssuerSchema = new Schema({
    name: {type: String, required: [true, 'Must specify a name'], trim: true},
    organization: {type: ObjectId, ref: 'Organization', required: [true, 'Must specify an organization'], index: true},
    url: {
        type: String,
        trim: true,
        match: [/^((?:https?:\/\/)?[^./]+(?:\.[^./]+)+(?:\/.*)?)$/, 'Please use a valid URL'],
        minlength: [6, 'URL can\'t be less than 6 characters'],
        maxlength: [256, 'URL can\'t be more than 64 characters']
    },
    image: {type: String},
    description: {type: String, trim: true},
});

const autoPopulate = function (next) {
    this.populate({path: 'organization'});
    next();
};

// noinspection JSUnresolvedFunction
IssuerSchema
    .pre('findOne', autoPopulate)
    .pre('find', autoPopulate);

// Return model
module.exports = mongoose.model('Issuer', IssuerSchema);
