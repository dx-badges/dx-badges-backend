// Dependencies
const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    cError = require('../helpers/cerror');

mongoose.Promise = require('bluebird');

const OrganizationSchema = new Schema({
    name: {type: String, required: [true, 'Must specify a name'], trim: true},
    url: {
        type: String,
        trim: true,
        match: [/^((?:https?:\/\/)?[^./]+(?:\.[^./]+)+(?:\/.*)?)$/, 'Please use a valid URL'],
        minlength: [6, 'URL can\'t be less than 6 characters'],
        maxlength: [256, 'URL can\'t be more than 64 characters'],
        required: [true, 'Must specify an URL']
    },
    email: { // TODO unique ??
        type: String,
        trim: true,
        lowercase: true,
        match: [/^([\w-.]+@([\w-]+\.)+[\w-]{2,4})?$/, 'Please use a valid email address'],
        minlength: [9, 'Email can\'t be less than 9 characters'],
        maxlength: [64, 'Email can\'t be more than 64 characters'],
        required: [true, 'Must specify an email']
    },
    image: {type: String},
    telephone: {type: Number},
    description: {type: String, trim: true},
});

// Return model
module.exports = mongoose.model('Organization', OrganizationSchema);
