// Dependencies
const mongoose = require('mongoose'),
    Schema = mongoose.Schema,
    ObjectId = mongoose.Types.ObjectId,
    cError = require('../helpers/cerror');

mongoose.Promise = require('bluebird');

const PersonSchema = new Schema({
    name: {type: String, required: [true, 'Must specify a name'], trim: true, index: true},
    security: {type: String, required: [true, 'Must specify a security code.']},
    email: {
        type: String,
        trim: true,
        lowercase: true,
        match: [/^([\w-.]+@([\w-]+\.)+[\w-]{2,4})?$/, 'Please use a valid email address'],
        minlength: [9, 'Email can\'t be less than 9 characters'],
        maxlength: [64, 'Email can\'t be more than 64 characters'],
        required: [true, 'Must specify an email'],
        index: {unique: true}
    },
    image: {type: String},
    telephone: {type: String},
    description: {type: String, trim: true},
    type: {
        type: String,
        enum: ['Admin', 'Manager', 'Profile'],
        default: 'Profile'
    },
    issuerOf: [{type: ObjectId, ref: 'Issuer'}]
});

// noinspection JSUnresolvedFunction
PersonSchema.post('save', function (error, doc, next) {
    if (error.code === 11000)
        next(new cError(403, 'Email already in use for another account.'));
    else next(error);
});

// noinspection JSUnresolvedFunction
PersonSchema.post('findOneAndUpdate', function (error, doc, next) {
    if (error.code === 11000)
        next(new cError(403, 'This email is associated with another account, please use a different one.'));
    else next(error);
});

// Return model
module.exports = mongoose.model('Person', PersonSchema);
