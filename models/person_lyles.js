// Dependencies

const mongoose = require('mongoose'),
    Schema = mongoose.Schema;

mongoose.Promise = require('bluebird');

const PersonDAO = require('./person');
const options = {discriminatorKey: 'person_type'};

const LylesCenterPersonSchema = PersonDAO.discriminator('LylesCenter',
    new Schema({
        schoolName: {type: String, trim: true, index: true},
        gradeLevel: {type: String, trim: true},
    }, options)
);

// Return model
module.exports = LylesCenterPersonSchema;
