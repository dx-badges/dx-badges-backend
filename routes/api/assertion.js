// Dependencies
const router = require('express').Router({}),
    mongoose = require('mongoose'),
    {printLogs, printErrors, sendEmail} = require("../../helpers/tools"),
    {asyncMiddleware, pagination} = require("../../helpers/middlewares"),
    cError = require('../../helpers/cerror'),

    Assertion = require('../../models/assertion'),
    {isManager, isAuthUser, getAuthUser} = require('../../helpers/authentication');

mongoose.Promise = require('bluebird');

/**
 *  +========== VARIABLES & CONSTANTS =============+
 */

router.get('/', isAuthUser, asyncMiddleware(async (req, res, next) => {
    printLogs('GET: assertions');
    let assertions = await Assertion.find({recipient: req.person._id}).populate('recipient badge').exec();
    return res.status(200).json(assertions)
}));

router.get('/:id', getAuthUser, asyncMiddleware(async (req, res, next) => {
    printLogs('GET: assertions/id | id:', req.params.id);
    let assertion = await Assertion.findById(req.params.id).populate('recipient badge').exec();
    return res.status(200).json(assertion)
}));

router.post('/:id/email', getAuthUser, asyncMiddleware(async (req, res, next) => {
    printLogs('GET: assertions/id | id:', req.params.id);
    let assertion = await Assertion.findById(req.params.id).populate('recipient badge').exec();

    const body =
        `<html lang="en">
            <head><title>FS Badges by DX</title></head>
            <body>
                <p>I just received my ${assertion.badge.name} badge! Check it out!</p>
                <br>
                <a href='${req.headers.origin}/wallet/${assertion._id}'>${req.headers.origin}/wallet/${assertion._id}</a>
            </body>
        </html>`
    try {
        await sendEmail(req.body.email, `${assertion.recipient.name} shared their badge with you.`, body)
    } catch (e) {
        console.log(e);
    }

    return res.status(200).json({message: 'Email send successfully'})
}));

router.delete('/:id', isManager, asyncMiddleware(async (req, res, next) => {
    printLogs('DELETE: assertions/id | id:', req.params.id);
    if (req.body.revocationReason) {
        let assertion = await Assertion.findByIdAndUpdate(req.params.id, {
            isRevoked: true,
            revocationReason: req.body.revocationReason
        }, {new: true}).exec();
        return res.status(200).json(assertion)
    } else {
        printErrors('Error: All information was not provided');
        res.status(500).json({message: 'All information was not provided.'});
    }
}));

// Return router
module.exports = router;
