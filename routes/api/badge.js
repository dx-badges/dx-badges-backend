// Dependencies
const router = require('express').Router({}),
    mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId,

    {printLogs, printErrors, sendEmail} = require("../../helpers/tools"),
    {asyncMiddleware, pagination} = require("../../helpers/middlewares"),
    cError = require('../../helpers/cerror'),

    Badges = require('../../models/badge'),
    Assertion = require('../../models/assertion'),
    {isManager, isAuthUser, getAuthUser} = require('../../helpers/authentication');

mongoose.Promise = require('bluebird');

/**
 *  +========== VARIABLES & CONSTANTS =============+
 */

router.post('/', isManager, asyncMiddleware(async (req, res) => {
    printLogs('POST: badges | name:', req.body.name);
    if (req.body.name && req.body.issuer) {
        const parameters = {};
        parameters.name = req.body.name;
        parameters.issuer = req.body.issuer;  // TODO take form person._id
        if (req.body.criteria) parameters.criteria = req.body.criteria;
        if (req.body.alignment) parameters.alignment = req.body.alignment;
        if (req.body.tags) parameters.tags = req.body.tags;
        if (req.body.expirationDate) parameters.expirationDate = req.body.expirationDate;
        if (req.body.image) parameters.image = req.body.image;
        if (req.body.description) parameters.description = req.body.description;
        let badge = await new Badges(parameters).save();
        res.status(201).json({badge: badge, message: `Badge "${badge.name}" created successfully.`});
    } else {
        printErrors('Error: All information was not provided');
        res.status(500).json({message: 'All information was not provided.'});
    }
}));

router.get('/', getAuthUser, pagination(), asyncMiddleware(async (req, res, next) => {
    printLogs('GET: badges');
    let badges = await Badges.find({}).skip(req.from).limit(req.limit).exec();
    return res.status(200).json(badges)
}));

router.get('/:id', getAuthUser, asyncMiddleware(async (req, res, next) => {
    printLogs('GET: badges/id | id:', req.params.id);
    let badges = await Badges.findById(req.params.id).exec();
    return res.status(200).json(badges)
}));

router.put('/:id', isManager, asyncMiddleware(async (req, res, next) => {
    // TODO - versioning system - HOLD
    return res.status(400).json({message: 'ON HOLD!'})
}));

router.delete('/:id', isManager, asyncMiddleware(async (req, res, next) => {
    printLogs('DELETE: badges/id | id:', req.params.id);
    let badges = await Badges.findByIdAndUpdate(req.params.id, {isDeleted: true}, {new: true}).exec();
    return res.status(200).json(badges)
}));

router.post('/:id/issue', isManager, asyncMiddleware(async (req, res, next) => {
    printLogs('POST: badges/id/issue | id:', req.params.id);

    if (!req.params.id || !req.body.recipient) {
        printErrors('Error: All information was not provided');
        return next(new cError(500, 'All information was not provided.'));
    }

    if (Array.isArray(req.body.recipient)) {
        if (req.body.recipient.length > 0) {
            let message = '';
            let assertions = await Assertion.find({
                badge: req.params.id,
                recipient: {$in: req.body.recipient}
            }).select('_id recipient').exec();
            if (assertions.length > 0) {
                req.body.recipient = req.body.recipient.filter((el) => !assertions.map(a => a.recipient.toString()).includes(el));
                message += `${assertions.length} badges were already issued. `;
            }
            assertions = [];
            for (const recipient of req.body.recipient) {
                const parameters = {};
                parameters.badge = req.params.id;
                parameters.recipient = recipient;
                if (req.body.verification) parameters.verification = req.body.verification;
                if (req.body.expires) parameters.expires = req.body.expires;
                if (req.body.evidence) parameters.evidence = req.body.evidence;
                if (req.body.narrative) parameters.narrative = req.body.narrative;
                assertions.push(parameters)
            }
            if (assertions.length > 0) {
                message += `${assertions.length} badges issued successfully. `;
                assertions = await Assertion.insertMany(assertions, {ordered: false});
                const _ids = assertions.map(assertion => assertion._id);
                if (_ids.length > 0) {
                    assertions = await Assertion.find({_id: {$in: _ids}}).populate('recipient badge').exec();

                    assertions.forEach(assertion => {
                        const body =
                            `<html lang="en">
                                <head><title>FS Badges by DX</title></head>
                                <body>
                                    <p>Congratulations on receiving the following badge ${assertion.badge.name}! We are proud of you and you should be too! Keep up the great work.</p>
                                    <br>
                                    <a href='${req.headers.origin}/wallet/${assertion._id}'>${req.headers.origin}/wallet/${assertion._id}</a>
                                </body>
                            </html>`
                        try {
                            sendEmail(assertion.recipient.email, 'Congratulations, you earned a badge!', body)
                        } catch (e) {
                            console.log(e);
                        }
                    })
                }
            }
            res.status(201).json({message: message ? message : 'Badges issued successfully.'});
        } else {
            printErrors('Error: At-least one recipient is required');
            res.status(500).json({message: 'At-least one recipient is required.'});
        }
    } else {
        let assertion = await Assertion.findOne({
            badge: req.params.id,
            recipient: req.body.recipient
        }).select('_id').exec();
        if (assertion) {
            res.status(406).json({assertion: assertion, message: 'Badge already issued.'});
        } else {
            const parameters = {};
            parameters.badge = req.params.id;
            parameters.recipient = req.body.recipient;
            if (req.body.verification) parameters.verification = req.body.verification;
            if (req.body.expires) parameters.expires = req.body.expires;
            if (req.body.evidence) parameters.evidence = req.body.evidence;
            if (req.body.narrative) parameters.narrative = req.body.narrative;
            assertion = await new Assertion(parameters).save();

            assertion = await Assertion.findById(assertion._id).populate('recipient badge').exec();

            const body =
                `<html lang="en">
                    <head><title>FS Badges by DX</title></head>
                    <body>
                        <p>Congratulations on receiving the following badge ${assertion.badge.name}! We are proud of you and you should be too! Keep up the great work.</p>
                        <br>
                        <a href='${req.headers.origin}/wallet/${assertion._id}'>${req.headers.origin}/wallet/${assertion._id}</a>
                    </body>
                </html>`
            try {
                await sendEmail(assertion.recipient.email, 'Congratulations, you earned a badge!', body)
            } catch (e) {
                console.log(e);
            }
            res.status(201).json({assertion: assertion, message: 'Badge issued successfully.'});
        }
    }
}));

router.get('/:id/recipients', isAuthUser, asyncMiddleware(async (req, res, next) => {
    printLogs('GET: badges/id/recipients | id:', req.params.id);
    let assertions = await Assertion.find({badge: req.params.id}).populate('recipient badge').exec();
    return res.status(200).json(assertions)
}));


// Return router
module.exports = router;
