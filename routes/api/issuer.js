// Dependencies
const router = require('express').Router({}),
    mongoose = require('mongoose'),
    {printLogs, printErrors} = require("../../helpers/tools"),
    {asyncMiddleware, pagination} = require("../../helpers/middlewares"),
    cError = require('../../helpers/cerror'),

    Organization = require('../../models/organization'),
    Issuer = require('../../models/issuer'),
    Badges = require('../../models/badge'),
    Assertion = require('../../models/assertion'),
    {isManager, isAuthUser, getAuthUser} = require('../../helpers/authentication');

mongoose.Promise = require('bluebird');

/**
 *  +========== VARIABLES & CONSTANTS =============+
 */

router.post('/', isManager, asyncMiddleware(async (req, res) => {
    printLogs('POST: issuers | name:', req.body.name);
    if (req.body.name && req.body.organization) {
        const org = await Organization.findById(req.body.organization).select('_id').exec();
        console.log(org);
        if (org && org._id) {
            const parameters = {};
            parameters.name = req.body.name;
            parameters.organization = org._id;
            if (req.body.url) parameters.url = req.body.url;
            if (req.body.image) parameters.image = req.body.image;
            if (req.body.description) parameters.description = req.body.description;
            let issuer = await new Issuer(parameters).save();
            res.status(201).json({issuer: issuer, message: `Issuer "${issuer.name}" created successfully.`});
        } else {
            printErrors('Error: Organization does not exist');
            res.status(500).json({message: 'Organization does not exist.'});
        }
    } else {
        printErrors('Error: All information was not provided');
        res.status(500).json({message: 'All information was not provided.'});
    }
}));

router.get('/', getAuthUser, pagination(), asyncMiddleware(async (req, res, next) => {
    printLogs('GET: issuers');
    let issuers = await Issuer.find({}).skip(req.from).limit(req.limit).exec();
    return res.status(200).json(issuers)
}));

router.get('/:issuer/badges', getAuthUser, pagination(), asyncMiddleware(async (req, res, next) => {
    printLogs('GET: issuers/badges');
    let badges = await Badges.find({issuer: req.params.issuer}).skip(req.from).limit(req.limit).lean().exec();
    for (const badge of badges) {
        badge['num'] = await Assertion.countDocuments({badge: badge._id});
    }
    return res.status(200).json(badges)
}));


// Return router
module.exports = router;
