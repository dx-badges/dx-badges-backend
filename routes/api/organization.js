// Dependencies
const router = require('express').Router({}),
    mongoose = require('mongoose'),
    {printLogs, printErrors} = require("../../helpers/tools"),
    {asyncMiddleware, pagination} = require("../../helpers/middlewares"),
    cError = require('../../helpers/cerror'),

    Organization = require('../../models/organization'),
    Issuer = require('../../models/issuer'),
    {isAdmin, isAuthUser, getAuthUser} = require('../../helpers/authentication');

mongoose.Promise = require('bluebird');

/**
 *  +========== VARIABLES & CONSTANTS =============+
 */

router.post('/', isAdmin, asyncMiddleware(async (req, res) => {
    printLogs('POST: organizations | name:', req.body.name);
    if (req.body.name && req.body.url && req.body.email) {
        const parameters = {};
        parameters.email = req.body.email;
        parameters.url = req.body.url;
        parameters.name = req.body.name;
        if (req.body.image) parameters.image = req.body.image;
        if (req.body.telephone) parameters.telephone = req.body.telephone;
        if (req.body.description) parameters.description = req.body.description;
        let organization = await new Organization(parameters).save();
        res.status(201).json({organization: organization, message: `Organization "${organization.name}" created successfully.`});
    } else {
        printErrors('Error: All information was not provided');
        res.status(500).json({message: 'All information was not provided.'});
    }
}));

router.get('/', getAuthUser, pagination(), asyncMiddleware(async (req, res, next) => {
    printLogs('GET: organizations');
    let organizations = await Organization.find({}).skip(req.from).limit(req.limit).sort('-_id').exec();
    return res.status(200).json(organizations)
}));

router.get('/:organization/issuers', getAuthUser, pagination(), asyncMiddleware(async (req, res, next) => {
    printLogs('GET: organizations/issuers');
    let issuers = await Issuer.find({organization: req.params.organization}).skip(req.from).limit(req.limit).sort('-_id').exec();
    return res.status(200).json(issuers)
}));

// Return router
module.exports = router;
