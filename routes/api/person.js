/**
 *  +=============== DEPENDENCIES ================+
 */
const router = require('express').Router({}),
    mongoose = require('mongoose'),
    sign = require('jsonwebtoken').sign,

    cError = require('../../helpers/cerror'),
    {jwtSecret, jwtIssuer, jwtAudience, personSecuredFields, personPublicFields} = require('../../helpers/config'),

    Person = require('../../models/person'),
    Assertion = require('../../models/assertion'),

    {printLogs, printInfo, printWarning, printErrors, mongoErrorMessage, generateSecurity, randomInteger, sendEmail} = require("../../helpers/tools"),
    {asyncMiddleware, pagination} = require('../../helpers/middlewares'),
    {isAdmin, isNotAuth, isAuthUser, getAuthUser} = require('../../helpers/authentication');

mongoose.Promise = require('bluebird');

/**
 *  +========== VARIABLES & CONSTANTS =============+
 */
const TOKEN_EXPIRE_IN = '30 days';
let isNewSystem = false;

/**
 *  +============== INITIALIZATION ================+
 */
const newSystem = () => {
    Person.findOne({}, '_id', (err, res) => {
        if (!res) {
            isNewSystem = true;
            printWarning('Server is in setup mode, waiting for admin registration.');
        }
    });
};

/**
 *  +================== USER ROUTES ================+
 */
router.post('/register', isNotAuth, asyncMiddleware(async (req, res, next) => {
    printLogs('POST: register');
    if (req.body.name && req.body.email) {
        const parameters = {};
        parameters.name = req.body.name;
        parameters.email = req.body.email;
        if (req.body.image) parameters.image = req.body.image;
        if (req.body.telephone) parameters.telephone = req.body.telephone;
        if (req.body.description) parameters.description = req.body.description;

        // generate secret
        parameters.security = generateSecurity(randomInteger(16, 32));
        console.log('Security:', parameters.security, 'Length:', parameters.security.length);

        if (isNewSystem) parameters.type = 'Admin';
        printLogs('\tCreating new person account | email:', parameters.email);

        try {
            const person = await new Person(parameters).save();
            if (person) {
                printLogs('\tnew person account created successfully.');
                if (isNewSystem) {
                    isNewSystem = false;
                    printInfo('Admin registration successful, setup mode off.');
                }
                const {image, ...others} = person.toObject()
                /*res.status(201).json({
                    message: 'Registration successful.',
                    user: person, // TODO .getSecuredFields(),
                    token: 'Bearer ' + sign(others, jwtSecret, {
                        expiresIn: TOKEN_EXPIRE_IN,
                        issuer: jwtIssuer,
                        audience: jwtAudience,
                    })
                });*/
                const token = 'Bearer ' + sign(others, jwtSecret, {
                    expiresIn: TOKEN_EXPIRE_IN,
                    issuer: jwtIssuer,
                    audience: jwtAudience,
                })
                const body =
                    `<html lang="en">
                        <head><title>FS Badges by DX</title></head>
                        <body>
                            <p>Hello ${person.name}, To sign-in to FS Badges by DX, please visit the link below. If you did not make this request, please ignore this e-mail.</p>
                            <br>
                            <a href='${req.headers.origin}/login?auth_origin=${token}'>${req.headers.origin}/login?auth_origin=${token}</a>
                        </body>
                    </html>`
                try {
                    await sendEmail(req.body.email, 'Your Badges at FSDX Sign-in Link', body)
                } catch (e) {
                    console.log(e);
                }
                res.status(200).json({message: 'Registration successful. Please check your email.'});

            } else {
                printErrors('NotFound', 'Person not found, an error occurred, please try again later.');
                res.status(404).json({message: 'Person not found, an error occurred, please try again later.'});
            }
        } catch (err) {
            printErrors(err.name, err.message);
            if (err.errors) res.status(400).json({message: mongoErrorMessage(err.errors)});
            else res.status(err.status ? err.status : 404).json({message: err.message});
        }
    } else {
        printErrors('NullError', 'All information not provided.');
        return next(new cError(400, 'All information not provided.'));
    }
}));

router.post('/login', asyncMiddleware(async (req, res) => {
    printLogs('POST: login | email:', req.body.email);
    if (req.body.email) {
        try {
            const person = await Person.findOne({email: req.body.email.toLowerCase()}).select(personSecuredFields).exec();
            if (person) {
                // TODO check secret
                printLogs('\tsuccessfully logged in');
                const {image, ...others} = person.toObject()
                /*res.status(200).json({
                    message: 'Welcome back.',
                    user: person, // TODO .getSecuredFields(),
                    token: 'Bearer ' + sign(others, jwtSecret, {
                        expiresIn: TOKEN_EXPIRE_IN,
                        issuer: jwtIssuer,
                        audience: jwtAudience,
                    })
                }); */
                const token = 'Bearer ' + sign(others, jwtSecret, {
                    expiresIn: TOKEN_EXPIRE_IN,
                    issuer: jwtIssuer,
                    audience: jwtAudience,
                })
                const body =
                    `<html lang="en">
                        <head><title>FS Badges by DX</title></head>
                        <body>
                            <p>Hello ${person.name}, To sign-in to FS Badges by DX, please visit the link below. If you did not make this request, please ignore this e-mail.</p>
                            <br>
                            <a href='${req.headers.origin}/login?auth_origin=${token}'>${req.headers.origin}/login?auth_origin=${token}</a>
                        </body>
                    </html>`
                try {
                    await sendEmail(req.body.email, 'Your Badges at FSDX Sign-in Link', body)
                } catch (e) {
                    console.log(e);
                }
                res.status(200).json({message: 'Check email'});

            } else {
                printErrors('NotFound', 'Person not found, an error occurred, please try again later.');
                res.status(404).json({message: 'Person not found, an error occurred, please try again later.'});
            }
        } catch (err) {
            printErrors(err.name, err.message);
            res.status(404).json({message: err.message});
        }
    } else {
        printErrors('NullError', 'All information not provided.');
        res.status(400).json({message: 'All information not provided.'});
    }
}));

router.get('/me', isAuthUser, (req, res) => {
    printLogs('GET: ping');
    const {image, ...others} = req.person.toObject()
    res.status(200).json({
        user: req.person,
        token: 'Bearer ' + sign(others, jwtSecret, {
            expiresIn: TOKEN_EXPIRE_IN,
            issuer: jwtIssuer,
            audience: jwtAudience,
        })
    });
});

// TODO router.patch('/me', isAuthUser, asyncMiddleware(async (req, res) => {
//  }))

router.get('/badges', isAuthUser, asyncMiddleware(async (req, res, next) => {
    printLogs('GET: persons/badges');
    let assertions = await Assertion.find({recipient: req.person._id}).exec();
    return res.status(200).json(assertions)
}));

router.get('/issuers', isAuthUser, asyncMiddleware(async (req, res, next) => {
    printLogs('GET: persons/issuers');
    const person = await Person.findById(req.person._id).select('issuerOf').populate('issuerOf').exec();
    return res.status(200).json(person.issuerOf)
}));

/**
 *  +============== STARTUP ================+
 */
// Check if new system
newSystem();

// Return router
module.exports = router;
