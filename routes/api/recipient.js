/**
 *  +=============== DEPENDENCIES ================+
 */
const router = require('express').Router({}),
    mongoose = require('mongoose'),
    ObjectId = mongoose.Types.ObjectId,

    Person = require('../../models/person'),
    LylesPerson = require('../../models/person_lyles'),
    Issuer = require('../../models/issuer'),
    Badge = require('../../models/badge'),
    Assertion = require('../../models/assertion'),

    cError = require('../../helpers/cerror'),
    {printLogs, printInfo, printWarning, printErrors, mongoErrorMessage, generateSecurity, randomInteger} = require("../../helpers/tools"),
    {asyncMiddleware, pagination, sort} = require('../../helpers/middlewares'),
    {isAdmin, isManager, getAuthUser} = require('../../helpers/authentication');

mongoose.Promise = require('bluebird');

router.post('/', isManager, asyncMiddleware(async (req, res, next) => {
    printLogs('POST: recipient');
    if ((req.body.name || (req.body.firstName && req.body.lastName)) && req.body.email) {
        const parameters = {};
        let isLyles = false;
        if (req.body.name) parameters.name = req.body.name;
        else parameters.name = req.body.lastName + ', ' + req.body.firstName;
        parameters.email = req.body.email;
        if (req.body.hasOwnProperty('image')) parameters.image = req.body.image;
        if (req.body.hasOwnProperty('telephone')) parameters.telephone = req.body.telephone;
        if (req.body.hasOwnProperty('description')) parameters.description = req.body.description;
        if (req.person.type === 'Admin' && req.body.type) {
            parameters.type = req.body.type;
        }

        if (req.body.hasOwnProperty('schoolName')) {
            parameters.schoolName = req.body.schoolName;
            isLyles = true;
        }
        if (req.body.hasOwnProperty('gradeLevel')) {
            parameters.gradeLevel = req.body.gradeLevel;
            isLyles = true;
        }

        parameters.security = generateSecurity(randomInteger(16, 32));
        printLogs('\tCreating new person account | email:', parameters.email);

        try {

            let person
            if (isLyles) person = await new LylesPerson(parameters).save();
            else person = await new Person(parameters).save();

            if (person) {
                printLogs('\tnew person account created successfully.');
                res.status(201).json({
                    message: `New account "${person.name}" created successfully.`,
                    recipient: person
                });
            } else {
                printErrors('NotFound', 'Person not found, an error occurred, please try again later.');
                res.status(404).json({message: 'Person not found, an error occurred, please try again later.'});
            }
        } catch (err) {
            printErrors(err.name, err.message);
            if (err.errors) res.status(400).json({message: mongoErrorMessage(err.errors)});
            else res.status(err.status ? err.status : 404).json({message: err.message});
        }
    } else if (Array.isArray(req.body) && req.body.length > 0) {
        const recipients = [];
        const emails = [];
        for (const recipient of req.body) {
            const parameters = {};
            if (recipient.name && recipient.email) {
                parameters.name = recipient.name;
                parameters.email = recipient.email;
                if (recipient.hasOwnProperty('image')) parameters.image = recipient.image;
                if (recipient.hasOwnProperty('telephone')) parameters.telephone = recipient.telephone;
                if (recipient.hasOwnProperty('description')) parameters.description = recipient.description;
                if (recipient.hasOwnProperty('schoolName')) parameters.schoolName = recipient.schoolName;
                if (recipient.hasOwnProperty('gradeLevel')) parameters.gradeLevel = recipient.gradeLevel;
                recipients.push(parameters);
                emails.push(recipient.email)
                parameters.security = generateSecurity(randomInteger(16, 32));
            }
        }

        const r_existing = await Person
            .find({email: {$in: emails}})
            .select('_id email name schoolName gradeLevel')
            .lean()
            .exec();
        for (const acc of r_existing) acc['account'] = 'Exists';

        try {
            const r = await LylesPerson.insertMany(recipients, {ordered: false, lean: true});
            res.status(201).json({message: 'All accounts created successfully.', recipient: r.concat(r_existing)});
        } catch (e) {
            res.status(201).json({
                message: 'Only new accounts created successfully.',
                recipient: e['insertedDocs'].concat(r_existing)
            });
        }
    } else {
        printErrors('NullError', 'All information not provided.');
        return next(new cError(400, 'All information not provided.'));
    }
}));

router.get('/', getAuthUser, pagination(), sort(), asyncMiddleware(async (req, res, next) => {
    printLogs('GET: persons | query:', req.query.search || 'n/a');
    let query, count = 0;
    if (req.query.search) query = {
        $or: [
            {name: {$regex: req.query.search, $options: 'i'}},
            {email: {$regex: req.query.search, $options: 'i'}},
            {schoolName: {$regex: req.query.search, $options: 'i'}}
        ]
    }
    else query = {};
    if (req.query.count) count = await Person.countDocuments(query);
    //console.log(query);
    //console.log(req.skip);
    //console.log(req.limit);
    //console.log(req.sort);
    let persons = await Person.find(query).collation({locale: "en"}).skip(req.skip).limit(req.limit).sort(req.sort).lean().exec();

    if (req.person.issuerOf.length > 0) {
        const badges = await Badge.find({issuer: {$in: req.person.issuerOf}}).select('_id -issuer').exec();
        for (let p of persons) {
            p['issuedBadges'] = await Assertion.find({
                recipient: p._id,
                badge: {$in: badges}
            }).populate('recipient badge').exec();
        }
    }
    return res.status(200).json({result: persons, count: count})
}));

router.patch('/:id/', isManager, asyncMiddleware(async (req, res, next) => {
    printLogs('PATCH: recipient');
    try {
        const parameters = {};
        let isLyles = false;
        if (req.body.name) parameters.name = req.body.name;
        if (req.body.hasOwnProperty('image')) parameters.image = req.body.image;
        if (req.body.hasOwnProperty('telephone')) parameters.telephone = req.body.telephone;
        if (req.body.hasOwnProperty('description')) parameters.description = req.body.description;
        if (req.person.type === 'Admin' && req.body.type) {
            parameters.type = req.body.type;
        }

        if (req.body.hasOwnProperty('schoolName') && req.body.schoolName) {
            parameters.schoolName = req.body.schoolName;
            isLyles = true;
        }
        if (req.body.hasOwnProperty('gradeLevel') && req.body.gradeLevel) {
            parameters.gradeLevel = req.body.gradeLevel;
            isLyles = true;
        }
        console.log(parameters);

        let person;
        if (isLyles) person = await LylesPerson.findByIdAndUpdate(req.params.id, parameters, {new: true}).exec();
        else person = await Person.findByIdAndUpdate(req.params.id, parameters, {new: true}).exec();

        console.log(person);

        if (person) {
            printLogs('\tperson account updated successfully.');
            res.status(201).json({
                message: `Account "${person.name}" updated successfully.`,
                recipient: person
            });
        } else {
            printErrors('NotFound', 'Person not found, an error occurred, please try again later.');
            res.status(404).json({message: 'Person not found, an error occurred, please try again later.'});
        }
    } catch (err) {
        printErrors(err.name, err.message);
        if (err.errors) res.status(400).json({message: mongoErrorMessage(err.errors)});
        else res.status(err.status ? err.status : 404).json({message: err.message});
    }
}));

router.post('/:id/issuers', isAdmin, asyncMiddleware(async (req, res, next) => {
    printLogs('POST: recipient/issuers');
    const types = ['Admin', 'Manager'];
    const issuer = await Issuer.findById(req.body.issuer).select('_id').exec();
    if (!issuer) return next(new cError(404, 'Issuer not found.'));
    let person = await Person.findById(req.params.id).select('type issuerOf').exec();
    if (!person) return next(new cError(404, 'Person not found.'));
    if (types.indexOf(person.type) === -1) person.type = 'Manager'
    if (person.issuerOf.indexOf(req.body.issuer) === -1) person.issuerOf.push(issuer._id)
    person = await person.save();
    return res.status(200).json({recipient: person, message: 'Program assigned'})
}));

// Return router
module.exports = router;
